# Использование официального образа Python 3.10
FROM python:3.10-slim

# Определение аргумента среды, по умолчанию используется prod
ARG ENVIRONMENT=prod

# Установка необходимых системных компонентов
RUN apt-get update && apt-get install -y \
    git \
    && rm -rf /var/lib/apt/lists/*

# Установка Poetry
RUN pip install --upgrade pip && \
    pip install poetry pandas

# Установка рабочей директории в контейнере
WORKDIR /app

# Копирование проекта в контейнер
COPY . .

# Отключение создания виртуального окружения в Poetry
RUN poetry config virtualenvs.create false

# Условная установка зависимостей
RUN if [ "$ENVIRONMENT" = "prod" ]; then poetry install --only main; \
    else poetry install; fi

# Если это среда разработки, установка pre-commit hooks
RUN if [ "$ENVIRONMENT" = "dev" ]; then poetry run pre-commit install; fi

CMD ["poetry", "run", "python", "src/main.py"]